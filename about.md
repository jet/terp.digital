---
layout: page
title: about
subtitle: "here is some text to see how it looks"
permalink: /about/
---

This is just me relearning HTML and CSS, and learning SASS and Jekyll

The layout of the site is/will be **very** reminsicent of [NixNet](https://nixnet.services).
I'm not experienced enough with web design to think of a different layout, and I
think it looks nice

Sorry for not opening sign ups, I just don't want the headache of personally
managing others' data

## services

{% for service in site.data.services %} [**{{ service.name }}**]({{ service.url }}) \| {% endfor %}
