# vim:ft=make
set positional-arguments

@testinginprodxd:
	git add .
	git commit -m "Update website ($(date -u))"
	git push origin master
	nix-shell --packages bundler --run 'bundle install --path ./vendor; bundle exec jekyll build --destination ../pages'
	bash -c "pushd ../pages; git add .; git commit -m \"Update website ($(date -u))\"; git push origin master; popd"
