---
layout: "post"
title: "farside.link dockerfile"
subtitle: "ads bad, information good"
date: 2023-03-29 20:00:00
permalink: "/farside/"
---

[farside.link](https://farside.link) is an excellent service for not overloading
privacy frontends for proprietary services, but sometimes
the main instance is down. So I made myself a Dockerfile.

Let's see if code blocks work in my garbage css:

_(edit: it breaks the page on narrow displays lol)_
```
FROM elixir:alpine

# Disable automatic updates by containrrr/watchtower
LABEL com.centurylinklabs.watchtower.enable="false"

RUN apk add zstd cronie git make g++
RUN git clone https://git.sr.ht/~benbusby/farside

WORKDIR farside
ENV MIX_ENV=cli

RUN mix local.hex --force
RUN mix local.rebar --force
RUN yes | mix deps.get

RUN mix release
RUN cp _build/cli/rel/bakeware/farside /usr/local/bin
RUN chmod +x /usr/local/bin/farside


# won't run unless this dir exists idk
RUN mkdir -pv /root/.cache/bakeware/.tmp

# Get the latest services.json before starting
CMD git pull && /usr/local/bin/farside
```

and the `docker-compose.yml`
```
services:
  app:
    hostname: farside
    container_name: farside
    build: .
    restart: always
    dns:
    # Using cloudflare for this container
    # just to reduce noise in my DNS logs
      - "1.0.0.2"
      - "1.1.1.2"
    ports:
      - "4001:4001"
```
