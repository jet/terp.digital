---
layout: "page"
title: "posts"
subtitle: "Pretending anyone cares what I have to say lol. \nalso whatever you do don't look at the css"
permalink: "/posts/"
---

<div class="feed">
  <ul>
  {% if site.posts %}
  {% for post in site.posts %}

      <li>
        <a href="{{ post.url }}">
          <span class="post-preview--title">{{ post.title | capitalize }}{%if post.subtitle %} - {{ post.subtitle }}{% endif %}</span><span class="post-preview--date">{{ post.date | date: '%d %b %Y - %I:%M %p' }}</span>
        <p class="post-preview--content">{{ post.content | strip_html | truncatewords: 34 }}</p>
        </a>
      </li>
  {% endfor %}
  {% else %}
  asdf
  {%endif %}
  </ul>
</div>
